#!/usr/local/bin/python3.4
# -*- coding: utf-8 -*-
__author__ = 'atlas'

#
#

import requests
from bs4 import BeautifulSoup
import os


def getSource(url):     # 读取完整页面 返回一个漫画名称和下载地址的mapping
    r = requests.get(url)
    soup = BeautifulSoup(r.text,'lxml')
    title = soup.title.string[:-27] # bs4 找 title
    return(title)

def getPageNumber(page_url):    # 通过下载地址判断一共有多少页
    allPage = []
    p = requests.get(page_url)
    pagesoup = BeautifulSoup(p.text,'lxml')
    page = pagesoup.find(attrs={'class':'wp-pagenavi'}) # 直接查找attrs判断页面
    if page == None:    # 如果page值为空则返回默认页面
        number_of_page = 0
        allPage.append(page_url)
        return allPage
    else:
        number_of_page = int(page.contents[0].contents[-3].string)  # page不为空时返回多少页面
        for i in range(number_of_page):
            allPage.append(page_url+'/'+str(i+1))
        return allPage


def getImglink(page):       # get图片直链
    imgdr = []
    p = requests.get(page)
    imgsoup = BeautifulSoup(p.text,'lxml')
    imglink = imgsoup.findAll('img')    # 找html中所有图片
    for y in imglink:
        if 'alt' in y.attrs:        # 剔除没有编号的图片
            imgdr.append(y['src'])
    return  imgdr



def downloadComic(comic_link):      # 下载图片
    imglist = []
    comic_page = getPageNumber(comic_link)
    for x in comic_page:
        tmp = getImglink(x)
        for y in tmp:
            imglist.append(y)
    for z in range(len(imglist)):     
        img = requests.get(imglist[z])
        if os.path.exists(str(z) + '.jpg'):
            print("跳过"+str(z) + '.jpg')
        else:
            print("正在下载" + str(z) + '.jpg')
            with open(str(z)+'.jpg', 'wb') as f: # 图片wb模式写入 binary
                f.write(img.content)

def getSourcePageNumber():
    source = requests.get('http://www.177pic.info/html/category/tt/page/1')
    sourcesoup = BeautifulSoup(source.text,'lxml')
    sourcepage = sourcesoup.find(attrs={'class':'wp-pagenavi'})
    source_page_number = int(sourcepage.contents[-2]['href'].split('/')[-1])
    return source_page_number


def main(): # main 模块
    url = input('输入url开始下载: ')


    title = getSource(url)
    print('正在下载')

    if (os.path.exists(title)) == False:
        os.mkdir(title)
    os.chdir(title)
    downloadComic(url)
    cmd = "convert *.jpg large.pdf"
    os.system(cmd)
#    compress = "gs -sDEVICE=pdfwrite -dNOPAUSE -dQUIET -dBATCH -dPDFSETTINGS=/screen -dCompatibilityLevel=1.4 -sOutputFile=" + title + ".pdf large.pdf"
#    os.system(compress)

if __name__ == '__main__':
    main()