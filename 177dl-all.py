#!/usr/local/bin/python3.4
# -*- coding: utf-8 -*-
__author__ = 'atlas'

#
#

import requests
from bs4 import BeautifulSoup
import os

def getSource111(url):     # 读取完整页面 返回一个漫画名称和下载地址的mapping
    r = requests.get(url)
    soup = BeautifulSoup(r.text,'lxml')
    link = soup.find_all('h2')  # bs4 找 h2 tag
    dl = []
    title = []
    for x in link:
        title.append(x.contents[0]['title'][13:]) # h2 tag 下还有其他tag读取内容
        dl.append(x.contents[0]['href'])
    comic = dict(zip(dl,title))
    return(comic)

def getSource(url):     # 读取完整页面 返回一个漫画名称和下载地址的mapping
    r = requests.get(url)
    soup = BeautifulSoup(r.text,'lxml')
    title = soup.title.string[:-27] # bs4 找 title
    return(title)

def getPageNumber(page_url):    # 通过下载地址判断一共有多少页
    allPage = []
    p = requests.get(page_url)
    pagesoup = BeautifulSoup(p.text,'lxml')
    page = pagesoup.find(attrs={'class':'wp-pagenavi'}) # 直接查找attrs判断页面
    if page == None:    # 如果page值为空则返回默认页面
        number_of_page = 0
        allPage.append(page_url)
        return allPage
    else:
        number_of_page = int(page.contents[0].contents[-3].string)  # page不为空时返回多少页面
        for i in range(number_of_page):
            allPage.append(page_url+'/'+str(i+1))
        return allPage


def getImglink(page):       # get图片直链
    imgdr = []
    p = requests.get(page)
    imgsoup = BeautifulSoup(p.text,'lxml')
    imglink = imgsoup.findAll('img')    # 找html中所有图片
    for y in imglink:
        if 'alt' in y.attrs:        # 剔除没有编号的图片
            imgdr.append(y['src'])
    return  imgdr



def downloadComic(comic_link):      # 下载图片
    imglist = []
    comic_page = getPageNumber(comic_link)
    for x in comic_page:
        tmp = getImglink(x)
        for y in tmp:
            imglist.append(y)
    for z in range(len(imglist)):     
        img = requests.get(imglist[z])
        if os.path.exists(str(z) + '.jpg'):
            print("跳过"+str(z) + '.jpg')
        else:
            print("正在下载" + str(z) + '.jpg')
            with open(str(z)+'.jpg', 'wb') as f: # 图片wb模式写入 binary
                f.write(img.content)

def getSourcePageNumber():
    source = requests.get('http://www.177pic.info/html/category/tt/page/1')
    sourcesoup = BeautifulSoup(source.text,'lxml')
    sourcepage = sourcesoup.find(attrs={'class':'wp-pagenavi'})
    source_page_number = int(sourcepage.contents[-2]['href'].split('/')[-1])
    return source_page_number


def main(): # main 模块
    if (os.path.exists('comic')) == False:
        os.mkdir('comic')
    os.chdir('comic')

    if (os.path.exists('url.txt')) == False:
        f = open('url.txt', 'a')
        f.write("")
        f.close()

    #url = 'http://www.177pic.info/html/2016/07/1215311.html' #input('输入url开始下载: ')
    url = 'http://www.177pic.info/html/category/tt'
    total_page = getSourcePageNumber()
    url_list = []
    for i in range(0, total_page):  # 根据记录选择开始页面
        url_list.append(url + '/page/' + str(i + 1))
    url_list.reverse()
    #    tmp = os.popen('ls').readlines()
    #    allcomic = []
    #    for i in tmp:
    #        allcomic.append(i[:-1]) # 读取目录列表，保存以便判断漫画是否下载
    #    del tmp

    page = 1
    for y in url_list:

        print('正在下载: ', y)
        with open('recode', 'w') as f:
            f.write(y)
        comic = getSource111(y)

        book = 1
        for x in comic:
            title = getSource(x)

            downloaded = False
            f = open('url.txt', 'r')
            for line in f:
                if title in line:
                    downloaded =True
            f.close()

            if downloaded ==False:
                print('正在下载'+title)

                if (os.path.exists(title)) == False:
                    os.mkdir(title)
                os.chdir(title)
                downloadComic(x)
                #cmd = "convert *.jpg large.pdf"
                #os.system(cmd)

                book = book + 1
                os.chdir('..')

                #完整下载完成再做记录
                f = open('url.txt', 'a')
                xxxurl = title + "\t" + x + "\n"
                f.write(xxxurl)
                f.close()
            else:
                print("跳过 " +title)
        page = page + 1

#    compress = "gs -sDEVICE=pdfwrite -dNOPAUSE -dQUIET -dBATCH -dPDFSETTINGS=/screen -dCompatibilityLevel=1.4 -sOutputFile=" + title + ".pdf large.pdf"
#    os.system(compress)

if __name__ == '__main__':
    main()
